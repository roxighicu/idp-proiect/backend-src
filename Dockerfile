FROM python:3.6
WORKDIR /app
COPY requirements.txt /app
RUN pip install -U setuptools
RUN pip install -r requirements.txt
COPY . /app
EXPOSE 5000
CMD ["python", "-u", "main.py"]
