import datetime
import sys
import requests
import json
import pika
from flask import Flask, json, Response, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import exc
from sqlalchemy.sql import func
from flask_cors import CORS
import base64


""" Constants for table names. """
POSTS_TABLENAME = 'Posts'

""" Constants for request methods. """
GET_METHOD = 'GET'
POST_METHOD = 'POST'

""" App init. """
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root@databaseidp:3306/database_schema'
db = SQLAlchemy(app)
cors = CORS()
cors.init_app(app=app, supports_credentials=True)

"""
Creates a column with nullable
property set to false.
"""


def NonNullableColumn(*args, **kwargs):
    kwargs.setdefault('nullable', False)
    return db.Column(*args, **kwargs)


class Post(db.Model):
    __tablename__ = POSTS_TABLENAME
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = NonNullableColumn(db.String(40))
    comment = NonNullableColumn(db.String(400))

    def __init__(self, _title, _comment):
        self.title = _title
        self.comment = _comment


"""
Adds a new user to the database
"""


@app.route('/new', methods=[POST_METHOD])
def add_new_post():
    # Get the body from request
    body = request.get_json(force=True)

    try:
        # Create new post
        new_post = Post(
            body['title'],
            body['comment'],
        )

        # Add the new post to database
        db.session.add(new_post)
        db.session.commit()

        # Publish to RMQ
        if 'ALERT' in body['comment']:
            connection = pika.BlockingConnection(
                pika.ConnectionParameters(host='rabbitmq'))
            channel = connection.channel()
            channel.queue_declare(queue='task_queue', durable=True)
            channel.basic_publish(
                exchange='',
                routing_key='task_queue',
                body=body['comment'],
                properties=pika.BasicProperties(
                    delivery_mode=2,
                ))
            connection.close()

        return Response(
            status=201,
            response=json.dumps({
                "title": new_post.title,
                "comment": new_post.comment,
            })
        )
    except exc.IntegrityError:
        return Response(status=409)


@app.route('/posts', methods=[GET_METHOD])
def get_all_posts():
    all_posts = Post.query.all()
    response = []

    for post in all_posts:
        response.append({
            'title': post.title,
            'comment': post.comment,
        })

    return Response(status=200, response=json.dumps(response))


if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
